// This is an independent project of an individual developer. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com


// The MIT License (MIT)

// Copyright 2020 Vitalii Minnakhmetov <restlessmonkey@ya.ru>

// Permission is hereby granted, free of charge, to any person obtaining a
// copy of this software and associated documentation files (the
// "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit
// persons to whom the Software is furnished to do so, subject to the
// following conditions:

// The above copyright notice and this permission notice shall be included
// in all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
// OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN
// NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
// OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE
// USE OR OTHER DEALINGS IN THE SOFTWARE.


#include <VcppBits/contrib/catch2/catch.hpp>


#include "CircularBuffers.hpp"


using namespace VcppBits::CircularBuffers;


TEST_CASE("CircularArray initialized", "[CircularBuffers]") {
    CircularArray<int, 10>  arr;
    CHECK(arr.size() == 0);
    CHECK(arr.getMaxSize() == 10);
    CHECK(arr.getUnderlyingContainer().size() == 10);
    CHECK(arr.getUnderlyingContainer()[0] == 0);
    CHECK(arr.getUnderlyingContainer()[9] == 0);
    CHECK_THROWS_AS(arr[0], std::out_of_range);
}


TEST_CASE("CircularArray filled with one element", "[CircularBuffers]") {
    CircularArray<int, 10>  arr;
    arr.push_back(1);
    CHECK(arr[0] == 1);
    CHECK(arr.size() == 1);
    CHECK(arr.getMaxSize() == 10);
    CHECK(arr.getUnderlyingContainer().size() == 10);
    CHECK(arr.getUnderlyingContainer()[0] == 1);
    CHECK(arr.getUnderlyingContainer()[9] == 0);
    CHECK_THROWS_AS(arr[1], std::out_of_range);
}

TEST_CASE("CircularArray continuously filled with data", "[CircularBuffers]") {
    std::vector<int> vec;
    vec.reserve(200);
    CircularArray<int, 10>  arr;
    for (int i = 0; i < 200; ++i) {
        vec.push_back(i);
        arr.push_back(i);

        for (size_t j = 0; j < arr.size(); ++j) {
            auto k = vec.size() - arr.size() + j;
            REQUIRE(arr[j] == vec[k]); // doing REQUIRE coz we don't want 100+
                                       // asserts to fill our log
        }
    }
    CHECK (arr.getMaxSize() == 10);
    CHECK (arr[9] == 199);
}


TEST_CASE("CircularVector initialized", "[CircularBuffers]") {
    CircularVector<int> arr(10);
    CHECK(arr.size() == 0);
    CHECK(arr.getMaxSize() == 10);
    CHECK_THROWS_AS(arr.getUnderlyingContainer().at(0), std::out_of_range);
    CHECK_THROWS_AS(arr[0], std::out_of_range);
}


TEST_CASE("CircularVector filled with one element", "[CircularBuffers]") {
    CircularVector<int> arr(10);
    arr.push_back(1);
    CHECK(arr[0] == 1);
    CHECK(arr.size() == 1);
    CHECK(arr.getMaxSize() == 10);
    CHECK(arr.getUnderlyingContainer().capacity() == 10);
    CHECK(arr.getUnderlyingContainer()[0] == 1);
    CHECK_THROWS_AS(arr[1], std::out_of_range);
}


TEST_CASE("CircularVector continuously filled with data", "[CircularBuffers]") {
    std::vector<int> vec;
    vec.reserve(200);
    CircularVector<int>  arr(10);
    for (int i = 0; i < 200; ++i) {
        vec.push_back(i);
        arr.push_back(i);

        for (size_t j = 0; j < arr.size(); ++j) {
            auto k = vec.size() - arr.size() + j;
            REQUIRE(arr[j] == vec[k]); // doing REQUIRE coz we don't want 100+
                                       // asserts to fill our log
        }
    }
    CHECK (arr.getMaxSize() == 10);
    CHECK (arr[9] == 199);
}
