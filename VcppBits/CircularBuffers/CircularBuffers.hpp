// The MIT License (MIT)

// Copyright 2020 Vitalii Minnakhmetov <restlessmonkey@ya.ru>

// Permission is hereby granted, free of charge, to any person obtaining a
// copy of this software and associated documentation files (the
// "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit
// persons to whom the Software is furnished to do so, subject to the
// following conditions:

// The above copyright notice and this permission notice shall be included
// in all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
// OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN
// NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
// OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE
// USE OR OTHER DEALINGS IN THE SOFTWARE.


#ifndef VCPPBITS_CIRCULAR_BUFFERS_HPP_INCLUDED
#define VCPPBITS_CIRCULAR_BUFFERS_HPP_INCLUDED

#include <vector>
#include <cstddef>
#include <array>
#include <stdexcept>

namespace VcppBits {
namespace CircularBuffers {

template<typename T>
class CircularVector {
public:
    explicit CircularVector (const size_t pLimit)
        : mLimit(pLimit) {
        mVector.reserve(pLimit);
    }
    void push_back (const T& pValue) {
        if (mVector.size() < mLimit) {
            mVector.push_back(pValue);
            ++end;
        } else {
            mVector[end++] = pValue;
        }

        if (end == mLimit) {
            end = 0;
        }

    }
    size_t size () const {
        return std::min(mVector.size(), mLimit);
    }

    T operator[] (const size_t pNum) const {
        if (pNum >= mVector.size()) {
            throw std::out_of_range("CircularVector operator[] index error");
        }

        const size_t num = end + pNum;

        return mVector[(calcBeginPos() + pNum) % mLimit];
    }

    const std::vector<T>& getUnderlyingContainer () const {
        return mVector;
    }

    size_t getMaxSize () {
        return mLimit;
    }

private:
    size_t calcBeginPos () const {
        return (mVector.size() == mLimit) ? end : 0;
    }

    std::vector<T> mVector;
    const size_t mLimit;

    size_t end = 0;
};


template<typename T, size_t N>
class CircularArray {
public:

    constexpr size_t getMaxSize () {
        return N;
    }

    void push_back (const T& pValue) {
        _data[_end++] = pValue;
        if (_end == N) {
            _end = 0;
        }

        if (_actualSize < N) {
            _actualSize++;
        }
    }

    /// unlike the underlying std::array, we be more strict
    const T& operator[] (const size_t pNum) const {
        if (pNum < _actualSize) {
            return _data[(calcBeginPos() + pNum) % N];
        }
        throw std::out_of_range("CircularArray operator[] index error");
    };

    size_t size () const {
        return _actualSize;
    }

    const std::array<T, N>& getUnderlyingContainer () const {
        return _data;
    }

private:
    size_t calcBeginPos () const {
        return (_actualSize == N) ? _end : 0;
    }

    std::array<T, N> _data;

    size_t _end = 0;

    size_t _actualSize = 0;
};

} // namespace CircularBuffers
} // namespace VcppBits


#endif // VCPPBITS_CIRCULAR_BUFFERS_HPP_INCLUDED
